<?php

namespace SpipLeague\Test\Component\Kernel;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Component\Kernel\Kernel;
use SpipLeague\Test\Component\Kernel\Stub\InstallationDetectorStub;
use Symfony\Component\DependencyInjection\ContainerBuilder;

#[CoversClass(Kernel::class)]
class KernelTest extends TestCase
{
    /**
     * @return array<string,array{expected: bool, backOffice: string, requestUri: string, scriptFilename: string, cwd: string}>
     */
    public static function dataIsBackOffice(): array
    {
        return [
            'nominal case 1' => [
                'expected' => true,
                'backOffice' => '/ecrire',
                'requestUri' => '/ecrire/?exec=acceuil',
                'scriptFilename' => 'unknown',
                'cwd' => 'unknown',
            ],
            'nominal case 2' => [
                'expected' => false,
                'backOffice' => '/ecrire',
                'requestUri' => '/?page=sommaire',
                'scriptFilename' => 'unknown',
                'cwd' => 'unknown',
            ],
        ];
    }

    #[DataProvider('dataIsBackOffice')]
    public function testIsBackOffice(
        bool $expected,
        string $backOffice,
        string $requestUri,
        string $scriptFilename,
        string $cwd,
    ): void {
        // Given
        $c = new ContainerBuilder();
        $c->set('spip.installation', InstallationDetectorStub::fromDir('dummy'));
        $c->setParameter('spip.dirs.core', 'core-test');
        $c->setParameter('spip.routes.back_office', $backOffice);
        $k = new Kernel($c, $requestUri, $scriptFilename, $cwd);

        // When
        $actual = $k->isBackOffice();

        // Then
        $this->assertSame($expected, $actual);
    }

    public function testBackOfficeNotSet(): void
    {
        // Given
        $this->expectException('RuntimeException');
        $c = new ContainerBuilder();
        $c->setParameter('spip.routes.back_office', '/test');
        $c->set('spip.installation', InstallationDetectorStub::fromDir('dummy'));

        // When
        (new Kernel($c, '', '', ''))->isBackOffice();

        // Then
        // An exception is thrown
    }

    public function testBackOfficeNotSetWithAString(): void
    {
        // Given
        $this->expectException('RuntimeException');
        $c = new ContainerBuilder();
        $c->setParameter('spip.routes.back_office', ['not-a-string-array']);

        // When
        (new Kernel($c, '', '', ''))->isBackOffice();

        // Then
        // An exception is thrown
    }

    public function testGetRootDir(): void
    {
        // Given
        $c = new ContainerBuilder();
        $c->set('spip.installation', InstallationDetectorStub::fromDir('dummy'));
        $c->setParameter('spip.routes.back_office', '/test');
        $c->setParameter('spip.dirs.core', 'core-test');
        $k = new Kernel($c, '/dummy', 'dummy', 'dummy');

        // When
        $actual = $k->getRootDir();

        // Then
        $this->assertStringEndsWith('tests/Fixtures/LegacySpip/', $actual);
    }

    public function testGetRelativeRootDir(): void
    {
        // Given
        $c = new ContainerBuilder();
        $c->set('spip.installation', InstallationDetectorStub::fromDir('dummy'));
        $c->setParameter('spip.routes.back_office', '/test');
        $c->setParameter('spip.dirs.core', 'core-test');
        $k = new Kernel($c, '/dummy', 'dummy', __DIR__ . '/Fixtures/LegacySpip/ecrire');

        // When
        $actual = $k->relative()
            ->getRootDir();

        // Then
        $this->assertEquals('../', $actual);
    }

    public function testGetCoreDir(): void
    {
        // Given
        $c = new ContainerBuilder();
        $c->set('spip.installation', InstallationDetectorStub::fromDir('dummy'));
        $c->setParameter('spip.routes.back_office', '/test');
        $c->setParameter('spip.dirs.core', 'core-test');
        $k = new Kernel($c, '/dummy', 'dummy', __DIR__ . '/Fixtures/LegacySpip');

        // When
        $actual = $k->getCoreDir();

        // Then
        $this->assertStringEndsWith('tests/Fixtures/LegacySpip/core-test/', $actual);
    }

    public function testGetRelativeTmpDir(): void
    {
        // Given
        $c = new ContainerBuilder();
        $c->set('spip.installation', InstallationDetectorStub::fromDir('dummy'));
        $c->setParameter('spip.routes.back_office', '/test');
        $c->setParameter('spip.dirs.core', 'core-test');
        $c->setParameter('spip.dirs.tmp', 'tmp-test');
        $k = new Kernel($c, '/dummy', 'dummy', __DIR__ . '/Fixtures/LegacySpip/core-test');

        // When
        $actual = $k->relative()
            ->getTmpDir();

        // Then
        $this->assertEquals('../tmp-test/', $actual);
    }
}
