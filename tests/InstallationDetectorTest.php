<?php

namespace SpipLeague\Test\Component\Kernel\Stub;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Component\Kernel\InstallationDetector;

#[CoversClass(InstallationDetector::class)]
class InstallationDetectorTest extends TestCase
{
    public function testFailFromDir(): void
    {
        // Given
        $this->expectException('RuntimeException');
        $this->expectExceptionMessage('SPIP Not fully installed');

        // When
        InstallationDetector::fromDir('does-not-exists');

        // Then
        // An exception is thrown
    }

    public function testFromDir(): void
    {
        // Given
        $installation = InstallationDetector::fromDir('tests/Fixtures/LegacySpip');

        // When
        $actual = $installation->getWay();

        // Then
        $this->assertEquals('legacy', $actual);
    }

    public function testGetProjectDir(): void
    {
        // Given
        $installation = InstallationDetector::fromDir('tests/Fixtures/LegacySpip');

        // When
        $actual = $installation->getDir();

        // Then
        $this->assertStringEndsWith('tests/Fixtures/LegacySpip/', $actual);
    }

    public function testGetUnknownDir(): void
    {
        // Given
        $installation = InstallationDetector::fromDir('tests/Fixtures/LegacySpip');

        // When
        $actual = $installation->getDir('unknown');

        // Then
        $this->assertStringEndsWith('tests/Fixtures/LegacySpip/', $actual);
    }

    public function testGetProjectConfigDir(): void
    {
        // Given
        $installation = InstallationDetector::fromDir('tests/Fixtures/LegacySpip');

        // When
        $actual = $installation->getDir('config');

        // Then
        $this->assertStringEndsWith('tests/Fixtures/LegacySpip/config/', $actual);
    }

    public function testFromComposer(): void
    {
        // Given
        \touch('spip.php');
        $installation = InstallationDetector::fromComposer();

        // When
        $actual = $installation->getWay();

        // Then
        $this->assertEquals('legacy', $actual);
        \unlink('spip.php');
    }

    /**
     * @return array<string,array{expected: string, directory: string}>
     */
    public static function dataGetSiteDir(): array
    {
        return [
            'legacy' => [
                'expected' => 'tests/Fixtures/LegacySpip/',
                'directory' => 'tests/Fixtures/LegacySpip',
            ],
            'standalone' => [
                'expected' => 'tests/Fixtures/StandaloneSpip/',
                'directory' => 'tests/Fixtures/StandaloneSpip',
            ],
            'mutualized' => [
                'expected' => 'tests/Fixtures/MutualizedSpip/sites/default/',
                'directory' => 'tests/Fixtures/MutualizedSpip',
            ],
        ];
    }

    #[DataProvider('dataGetSiteDir')]
    public function testGetSiteDir(string $expected, string $directory): void
    {
        // Given
        $installation = InstallationDetector::fromDir($directory);

        // When
        $actual = $installation->getDir('site');

        // Then
        $this->assertNotEmpty($expected);
        $this->assertStringEndsWith($expected, $actual);
    }

    /**
     * @return array<string,array{expected: string, directory: string}>
     */
    public static function dataGetWebDir(): array
    {
        return [
            'legacy' => [
                'expected' => 'tests/Fixtures/LegacySpip/',
                'directory' => 'tests/Fixtures/LegacySpip',
            ],
            'standalone' => [
                'expected' => 'tests/Fixtures/StandaloneSpip/public/',
                'directory' => 'tests/Fixtures/StandaloneSpip',
            ],
            'mutualized' => [
                'expected' => 'tests/Fixtures/MutualizedSpip/sites/default/public/',
                'directory' => 'tests/Fixtures/MutualizedSpip',
            ],
        ];
    }

    #[DataProvider('dataGetWebDir')]
    public function testGetWebDir(string $expected, string $directory): void
    {
        // Given
        $installation = InstallationDetector::fromDir($directory);

        // When
        $actual = $installation->getDir('web');

        // Then
        $this->assertNotEmpty($expected);
        $this->assertStringEndsWith($expected, $actual);
    }
}
