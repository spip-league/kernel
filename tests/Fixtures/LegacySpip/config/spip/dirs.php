<?php

/**
 * Répertoires physiques de l'installation SPIP.
 */
return [
    /**
     * Configuration permanente et inaccessible d'un site (clés, accès bdd, ...)
     *
     * Dans les cas ou l'instance du projet est configuré en Installation Legacy ou Standalone,
     * ce dossier est commun avec le dossier de commun avec celui de la configuration du framework.
     */
    'etc' => 'config/',
    /**
     * Fond documentaire permanent et accessible d'un site (logo, document, ...)
     */
    'doc' => 'IMG/',
    /**
     * Fichiers temporaires et inaccessibles d'un site (cache, session, log, ...)
     */
    'tmp' => 'tmp/',
    /**
     * Fichiers temporaires et accessibles d'un site (assets compressés, minifiés, images générés, ...)
     */
    'var' => 'local/',
    /**
     * Noyau historique de SPIP
     */
    'core' => 'ecrire/',
    /**
     * Plugins SPIP fournis avec une distribution, activés automatiquement
     */
    'extensions' => 'plugins-dist/',
    /**
     * Plugins SPIP installables pour l'instance
     */
    'plugins' => 'plugins/',
    /**
     * Jeu de Squelette SPIP par défaut de la distribution
     */
    'template' => 'squelettes-dist/',
    /**
     * Personalisaation de la distribution
     */
    'custom' => 'squelettes/',
    /**
     * Jeu de Squelette de l'espace privé SPIP par défaut de la distribution
     */
    'private_template' => 'prive/',

    /**
     * Répertoire des caches.
     */
    'cache' => '%spip.dirs.tmp%/cache/',
    /**
     * Répertoire des log.
     */
    'log' => '%spip.dirs.tmp%/log/',
    /**
     * Répertoire des sessions.
     */
    'sesssions' => '%spip.dirs.tmp%/sessions/',
];
