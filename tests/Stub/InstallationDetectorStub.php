<?php

namespace SpipLeague\Test\Component\Kernel\Stub;

use SpipLeague\Component\Kernel\InstallationDetectorInterface;

class InstallationDetectorStub implements InstallationDetectorInterface
{
    public static function fromDir(string $directory): self
    {
        return new self();
    }

    public function getWay(): string
    {
        return 'stub';
    }

    public function getDir(string $dir = 'project'): string
    {
        return (string) \realpath(__DIR__ . '/../Fixtures/LegacySpip');
    }
}
