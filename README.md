# spip-league/kernel

Noyau d'une application SPIP.

## Conteneur de service

- [Conteneur de services symfony](https://symfony.com/doc/current/service_container.html)
- [Ddocumentation de développement](https://symfony.com/doc/current/components/dependency_injection.html)

### Paramètres attendus par SPIP

- `spip.dirs.core`, le chemin relatif du dossier du noyau de SPIP
- `spip.routes.back_office`, la route HTTP d'accès à l'espace privé

### Service attendu par SPIP

- `spip.installation`, le détecteur de la forme d'installation de l'application.

## Détection de l'installation d'une applicagtion SPIP

La classe [InsstallationDetector](src/InstallationDetector.php) reconnaît 3 formes d'installation altenratives
d'une application SPIP.

### Legacy

À la racine de SPIP, le fichier `spip.php` est présent.
C'est la forme d'installation historique et par défaut.
`spip.php` est le point d'entrée du site public. On estime que s'il est présent,
alors le fichier `ecrire/index.php`, point d'entrée de l'espace privé, est présnt aussi.

### Standalone

En projet.

Si le fichier `spip.php` est absent, mais que le fichier `public/index.php` est présent,
c'est une nouvelle forme d'installation, expérimentale pour le moment,
qui permet de faire pointer le site web dans le sous-dossier `public/` et ainsi de sécuriser
l'ensemble des fichiers de l'application contre des appels intempestifs en dehors de ses points d'entrée.

### Mutualized

En projet.

Si le fichier `public/index.php` est absent, mais qu'il existe un fichier `sites/default/public/index.php`,
c'est une installation mutualisée.
