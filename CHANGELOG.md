# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- CI

### Fixed

- Traiter les scalaires seulement dans la récupération de paramètres
- Certains typages & phpdoc

## 2.0.2 - 2024-12-04

### Fixed

- Kernel testable

## 2.0.1 - 2024-11-30

### Changed

- Dépendances symfony 7.2 minimum.

## 2.0.0 - 2024-11-27

### Added

- `InstallationDetector` détecte comment est installée une application SPIP.
- `symfony/filesystem` >= 7.1 pour la gestion des chemins relatifs

### Changed

- PHP >= 8.2
- `symfony/dependency-injection` >= 7.1
