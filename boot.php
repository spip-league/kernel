<?php

namespace SpipLeague\Component\Kernel;

use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Raccourci temporaire pour accéder à l’application SPIP.
 */
function app(): Kernel
{
    static $installation = null;
    static $container = null;
    static $kernel = null;

    if ($installation === null) {
        $env = getenv('APP_ENV') ?: 'prod';

        $installation = $env === 'test' ?
            InstallationDetector::fromDir(\getcwd() . '/' . (getenv('TEST_DIR') ?: '')) :
            InstallationDetector::fromComposer();
    }

    if ($container === null) {
        $container = new ContainerBuilder();

        foreach (['dirs', 'filesystem', 'logger', 'routes'] as $config) {
            $file = $installation->getDir('config') . 'spip/' . $config . '.php';
            if (\file_exists($file)) {
                $fromFile = require_once $file;
                foreach ($fromFile as $key => $value) {
                    $container->setParameter(
                        'spip.' . $config . '.' . $key,
                        \call_user_func(function ($value) use ($container) {
                            if (\is_scalar($value) && \preg_match_all(',%[^%]+%,', $value, $matches)) {
                                foreach (\array_pop($matches) as $match) {
                                    $value = str_replace(
                                        $match,
                                        $container->getParameter(str_replace('%', '', $match)),
                                        $value,
                                    );
                                }
                            }

                            return $value;
                        }, $value),
                    );
                }
            }
        }

        $container->set('spip.installation', $installation);
    }

    if ($kernel === null) {
        $kernel = new Kernel(
            $container,
            $_SERVER['REQUEST_URI'] ?? '/',
            $_SERVER['SCRIPT_FILENAME'],
            (string) \getcwd(),
        );
    }

    return $kernel;
}

/**
 * Raccourci temporaire pour accéder à un paramètre du container symfony.
 *
 * @return mixed
 */
function param(string $name)
{
    return app()->getContainer()
        ->getParameter($name);
}

/**
 * Raccourci temporaire pour accéder à un service du container symfony.
 */
function service(string $id): object
{
    return app()->getContainer()
        ->get($id);
}
